import React, { Component } from 'react';
import './App.css';
import Header from '../Header/index'
import Section from '../Section/index';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Section/>
      </div>
    );
  }
}

export default App;
